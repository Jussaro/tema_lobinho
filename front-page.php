<?php
// Template Name: home
?>
<?php get_header()?>
    <section id="s1">
        <div class="boxtxt">
            <h1>Adote um Lobinho </h1>
            <hr class="text-hr">
            <p>É claro que o consenso sobre a necessidade de qualificação apresenta tendências no sentido de aprovar a
                manutenção das regras de conduta normativas.</p>
        </div>
    </section>
    <section id="s2">
        <h2>Sobre</h2>
        <p id="s2txt">Não obstante, o surgimento do comércio virtual faz parte de um processo de gerenciamento do
            levantamento das variáveis envolvidas. Não obstante, o surgimento do comércio virtual faz parte de um
            processo de gerenciamento do levantamento das variáveis envolvidas.Não obstante, o surgimento do comércio
            virtual faz parte de um processo de gerenciamento do levantamento das variáveis envolvidas.Não obstante, o
            surgimento do comércio virtual faz parte de um processo de gerenciamento do levantamento das variáveis
            envolvidas.</p>
    </section>
    <section id="s3">
        <h2>Valores</h2>
        <div id="valoresbox">
            <div class="vbox">
                <div class="vimg"><img src="<?php echo get_stylesheet_directory_uri() ?>/media/vimg-1.png" alt=""
                        class="real-img"></div>
                <h3>Proteção</h3>
                <p>Assim mesmo, o desenvolvimento contínuo de distintas formas de atuação facilita a criação do sistema
                    de participação geral.</p>
            </div>
            <div class="vbox">
                <div class="vimg"><img src="<?php echo get_stylesheet_directory_uri() ?>/media/vimg-2.png" alt=""
                        class="real-img"></div>
                <h3>Carinho</h3>
                <p>Assim mesmo, o desenvolvimento contínuo de distintas formas de atuação facilita a criação do sistema
                    de participação geral.</p>
            </div>
            <div class="vbox">
                <div class="vimg"><img src="<?php echo get_stylesheet_directory_uri() ?>/media/vimg-3.png" alt=""
                        class="real-img"></div>
                <h3>Companheirismo</h3>
                <p>Assim mesmo, o desenvolvimento contínuo de distintas formas de atuação facilita a criação do sistema
                    de participação geral.</p>
            </div>
            <div class="vbox">
                <div class="vimg"><img src="<?php echo get_stylesheet_directory_uri() ?>/media/vimg-4.png" alt=""
                        class="real-img"></div>
                <h3>Resgate</h3>
                <p>Assim mesmo, o desenvolvimento contínuo de distintas formas de atuação facilita a criação do sistema
                    de participação geral.</p>
            </div>
        </div>
    </section>
    <section id="s4">
        <h2>Lobos Exemplo</h2>
        <div id="lobospai">
            <div class="lobos-l">
                <div class="blueshadow">
                    <div class="foto"><img id="imagem" src="" alt="lobo"></div>
                </div>
                <div class="lobos-txt">
                    <h3 id="nome"></h3>
                    <p id="idade"></p>
                    <p id="descricao"></p>
                </div>
            </div>
            <div class="lobos-r">
                <div class="blueshadow">
                    <div class="foto"><img id="image" src="" alt="lobo"></div>
                </div>
                <div class="lobos-txt">
                    <h3 id="nom"></h3>
                    <p id="idad"></p>
                    <p id="descrica"></p>
                </div>
            </div>
        </div>
    </section>
    <script src="script.js"></script>
<?php get_footer(); ?>