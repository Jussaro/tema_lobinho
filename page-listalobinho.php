<?php
// Template Name: listalobinho
?>

<?php get_header()?>
    <section id="s4">
        <div id="lobospai">
            <div class="btn-input"><input type="text" class="input-txt"><button class="plobo">+ Lobo</button></div>
            <div style="margin:0px 0px 25px 25px"><input type="checkbox" name="" id="checkbox">Ver lobinhos adotados</div>
            <div class="lobos-l">
                <div class="blueshadow">
                    <div class="fotoslobo-l"><img src="" alt="Foto de um Lobo" id="img1"></div>
                </div>
                <div class="lobos-txt-r">
                    <div class="btninfo">
                        <a href="../adotar-lobinho/index.html"><button class="adotar">Adotar</button></a>
                    </div>
                    <div class="nomeage">
                        <h3 class="nome1"></h3>
                        <p class="age1"></p>
                    </div>
                    <p class="desc1"></p>
                </div>
            </div>
            <div class="lobos-r">
                <div class="blueshadow">
                    <div class="fotoslobo-r"><img src="" alt="Foto de um Lobo" id="img2"></div>
                </div>

                <div class="lobos-txt-l">
                    <div class="btninfo">
                        <a href="../adotar-lobinho/index.html"><button class="adotar">Adotar</button></a>
                    </div>
                    <div class="nomeage">
                        <h3 class="nome2"></h3>
                        <p class="age2"></p>
                    </div>
                    <p class="desc2"></p>
                </div>
            </div>
            <div class="lobos-l">
                <div class="blueshadow">
                    <div class="fotoslobo-l"><img src="" alt="Foto de um Lobo" id="img3"></div>
                </div>

                <div class="lobos-txt-r">
                    <div class="btninfo">
                        <a href="../adotar-lobinho/index.html"><button class="adotar">Adotar</button></a>
                    </div>
                    <div class="nomeage">
                        <h3 class="nome3"></h3>
                        <p class="age3"></p>
                    </div>
                    <p class="desc3"></p>
                </div>
            </div>
            <div class="lobos-r">
                <div class="blueshadow">
                    <div class="fotoslobo-r"><img src="" alt="Foto de um Lobo" id="img4"></div>
                </div>
                <div class="lobos-txt-l">
                    <div class="btninfo">
                        <a href="../adotar-lobinho/index.html"><button class="adotar">Adotar</button></a>
                    </div>
                    <div class="nomeage">
                        <h3 class="nome4"></h3>
                        <p class="age4"></p>
                    </div>
                    <p class="desc4"></p>
                </div>
            </div>
        </div>
    </section>
    <script src="script.js"></script>
</body>

<?php get_footer(); ?>