<!DOCTYPE html>
<html lang="pt-br">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="<?php echo get_stylesheet_directory_uri() ?>/style.css"> 
    <link rel="shortcut icon" href="../media/favicon.png" type="image/x-icon">
    <title>Homepage - Projeto Lobinho</title>
    <?php wp_head(); ?>
</head>

<body>
    <header>
        <a href="../lista-lobinhos/index.html">Nossos Lobinhos</a>
        <img src="<?php echo get_stylesheet_directory_uri() ?>/media/favicon.png" alt="Logo">
        <a href="../quem-somos/index.html">Quem somos</a>
    </header>